using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;

class GF3UnoScuro extends Ui.WatchFace {
    var pi12 = Math.PI / 12.0;
    var pi30 = Math.PI / 30.0;
    var pi50 = Math.PI / 50.0;
    var pi6  = Math.PI / 6.0;
    var pi7  = Math.PI / 3.5;
    var cx;
    var cy;
    var width;
    var height;
    var hourHand = new [7];
    var bckgrnd;
    var counter = 0;
    var showDay = true;

    //! Constructor
    function initialize() {
        bckgrnd = Ui.loadResource(Rez.Drawables.bckgrnd);
        hourHand = [ [-3,20], [-6,3], [-6,-3], [-2,-10], [-1,-105], [1,-105], [2,-10], [6,-3], [6,3], [3,20], [0,22] ];
    }

    function onLayout(dc) {
        width = dc.getWidth();
        height = dc.getHeight();
        cx = width / 2;
        cy = height / 2;
        onUpdate(dc);
    }

    function onShow() {}

    function onHide() {}

    function onExitSleep() {
        counter = 0;
    }

    function onEnterSleep() {
        showDay = true;
        onUpdate(dc);
    }

    function onUpdate(dc) {
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_BLACK, Gfx.COLOR_BLACK);
        dc.clear();
        dc.drawBitmap(-1, -1, bckgrnd);
        var hour= Sys.getClockTime().hour;
        var min = Sys.getClockTime().min;
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_SHORT);

        var txt;
        if (showDayOrMinute()) {
            txt = info.day.format("%02d");
            dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        } else {
            txt = min.format("%02d");
            dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        }
        dc.drawText(cx, cy+67, Gfx.FONT_MEDIUM, txt, Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);

        // Draw the hour hand
        hour = hour + (min / 60.0);
        hour = (hour * pi12) - Math.PI;
        drawHand(dc, hour, cx, cy, hourHand, Gfx.COLOR_WHITE);

        // Draw circle
        dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 7);
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 3);
    }

    function showDayOrMinute() {
        counter = counter + 1;
        if (counter > 2) {
            counter = 0;
            return !showDay;
        }
        return showDay;
    }

    function drawHand(dc, angle, centerX, centerY, coords, color) {
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + centerX;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + centerY;
            result[i] = [x, y];
        }
        dc.setColor(color, Gfx.COLOR_TRANSPARENT, color);
        dc.fillPolygon(result);
    }
}
